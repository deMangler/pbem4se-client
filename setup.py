# encoding: utf8
# setup.py
import sys
from distutils.core import setup

# This will only work on windows - for building dist exe
import py2exe  # noqa

sys.argv.append("py2exe")

setup(
    windows=["pbem_client.py"],
    data_files=[
        (
            "",
            [
                "pbem4se.ui",
                "pbem4se_settings.json",
                "readme.md",
                "readme.txt",
                "LICENSE"
            ],
        ),
        (
            "saves",
            [
                "saves/testsave.se1",
            ],
        ),
    ],
    options={
        "py2exe": {
            "bundle_files": 1,
            "compressed": True,
            "includes": [
                "pygubu.builder.tkstdwidgets",
                "pygubu.builder.ttkstdwidgets",
                "pygubu.builder.widgets.dialog",
                "pygubu.builder.widgets.editabletreeview",
                "pygubu.builder.widgets.scrollbarhelper",
                "pygubu.builder.widgets.scrolledframe",
                "pygubu.builder.widgets.tkscrollbarhelper",
                "pygubu.builder.widgets.tkscrolledframe",
                "pygubu.builder.widgets.pathchooserinput",
                "pygubu.builder.widgets.combobox",
            ],
        }
    },
)
