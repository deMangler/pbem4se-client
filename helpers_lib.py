"""
Some Helper bits and bobs,

Some thanks to stack overflow over the years...
"""
import json
import logging
import os
import re


# use cutelog
from logging.handlers import SocketHandler

logger = logging.getLogger("pbem4se_application")
logger.setLevel(1)  # to send all messages to cutelog
socket_handler = SocketHandler("127.0.0.1", 19996)  # default listening address
logger.addHandler(socket_handler)
logger.info("Hello world!")


def is_md5_hash(testhash):
    try:
        isit = re.findall(r"([a-fA-F\d]{32})", testhash) and len(testhash) == 32
    except:
        isit = False
    return isit


def is_token_hash(testhash):
    try:
        isit = re.findall(r"([a-fA-F\d]{40})", testhash) and len(testhash) == 40
    except:
        isit = False
    return isit


def wrap(value):
    if isinstance(value, dict):
        return DictObj(value)
    if isinstance(value, (tuple, list)):
        return ListObj(value)
    return value


class ListObj(object):
    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, key):
        return wrap(self.obj[key])


class DictObj(object):
    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, key):
        return wrap(self.obj[key])

    def __getattr__(self, key):
        try:
            return wrap(getattr(self.obj, key))
        except AttributeError:
            try:
                return self[key]
            except KeyError:
                raise AttributeError(key)


class TkRepeatingTask:
    def __init__(self, tkRoot, taskFuncPointer, freqencyMillis):
        self.__tk_ = tkRoot
        self.__func_ = taskFuncPointer
        self.__freq_ = freqencyMillis
        self.__isRunning_ = False

    def isRunning(self):
        return self.__isRunning_

    def start(self):
        self.__isRunning_ = True
        self.__onTimer()

    def stop(self):
        self.__isRunning_ = False

    def __onTimer(self):
        if self.__isRunning_:
            self.__func_()
            self.__tk_.after(self.__freq_, self.__onTimer)


class Settings(object):
    """
    handles all related to json settings file on the disk
    """

    def __init__(self, client):
        # i guess, check for settings and load them, and stuff
        # establish sane defaults for problematic failures, so at least gui works etc
        self.client = client

        self.fallback_values_dict = {
            "Authorization": "placeholder",
            "TestAuthorization": "f9de0cb3df5f3c85c246b46b8fca889d75a452d1",
            "SaveDir": "saves",
            "ServerURL": "127.0.0.1:8000",
            "GamePrefix": "default",
            "SaveDetectionInterval": "2000",
            "SaveDetectionThreshold": "5",
        }

        self.check_or_create_file()

        self.values_dict = {}
        self.values = None
        log_message = self.load_from_file()
        client.update_log_text(log_message)

        # self.save_to_file()

    def load_from_file(self, filepath="pbem4se_settings.json"):
        """
        load settings from json
        :return:
        """
        log_message = "\nAttempting to Load settings..."
        try:
            with open(filepath, "r") as f:

                try:
                    temp_json = json.load(f)

                    # lets try to add these to ourself
                    for k, v in temp_json.items():
                        setattr(self, k, v)
                        logger.debug(f"Setting: {k} to {v}")

                    self.SaveDetectionInterval = int(self.SaveDetectionInterval)
                    self.SaveDetectionThreshold = int(self.SaveDetectionThreshold)

                    # lets try to keep the values sane...
                    if self.SaveDetectionInterval < 2000:
                        self.SaveDetectionInterval = 2000

                    if self.SaveDetectionThreshold < 5:
                        self.SaveDetectionThreshold = 5

                    logger.debug(f"Loaded initial settings.")
                    log_message = log_message + 'Success.'

                except Exception as e:
                    log_message = log_message + f'failed to parse settings:\n{e}.'
                    logger.warning(f"failed to parse settings:{e}")

        except Exception as e:
            log_message = log_message + 'Failed to load.'

            logger.warning(f"failed to load settings:{e}")
        return log_message



    def save_to_file(self):
        """
        load settings from json
        tgcdo
        :return:
        """
        pass
        # try:
        #     # lets create a backup
        #     shutil.move("pbem4se_settings.json", "pbem4se_settings.bak")
        #
        #     with open("pbem4se_settings.json", "w") as f:
        #
        #         try:
        #             # temp_json = json.dumps(self.)
        #             with open("data.json", "w", encoding="utf-8") as f:
        #                 json.dump(self.values_dict, f, ensure_ascii=False, indent=4)
        #
        #             logger.debug(f"Saved settings to file.")
        #
        #         except Exception as e:
        #
        #             logger.error(f"failed to parse while saving settings:{e}")
        #
        # except Exception as e:
        #
        #     logger.warning(f"failed to save settings:{e}")

    def check_or_create_file(self):
        if os.path.exists("pbem4se_settings.json"):
            logger.debug(f"Default settings File Exists...")
        else:
            logger.warning(f"Default settings File Does Not Exist...")
            # code to generate placeholder values or option to load from other file.
