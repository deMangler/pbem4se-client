PBEM4SE
-------

A helper client for playing Shadow Empire PBEM games.



**What it does**

Enables seamless PBEM game play for Shadow Empire. Once set up, all players have to do is play the game normally. 
Saves are exchanged between players automatically.

**How it does it**

A multiplayer game is configured on the server using a web page, where details such as number of players and turn order
are specified. Once configured, the various players clients co-ordinate through the server to maintain game saves. 
Each player's client is authorised to their secure account through an SHA-1 token to ensure only they can take their 
turns and the game saves are safe.

**No, really, how does it do it?**

It creates a specifically named blank file in your save folder, eg, yourname_b8a8_round1_overwrite_me.se1. When you save after your turn
you overwrite this file, it notices, and will upload this to the server. 
Then next player will receive this in their save folder as theirname_b8a8_round2_load_me.se1. which they load and play, 
they will have a file like theirname_b8a8_round2_overwrite_me.se1, to save their game over, and so-on.

**Important!!! Make sure you load/overwrite the correct save, especially if you have more than one game on the go. the second part of the filename uniquely identifies the game.**
 
**Installing and setting up the client**
Unzip to a folder where you have write access. If you are using windows then pbem4se_client.exe should just run. 
The python version, once set up should run on most platforms.

You will need to give the client your unique authorisation token from the client settings page on the website. This can be done with "menu->server->change user", and tell
it where to look for save game files using "menu->file->Choose Savefile Dir", within the client, or editing the pbem4se_settings.json file directly for the settings to persist.

Everything should be simple enough, the main points are, be a player in an activated game on the server - 
have your token from the server imported into your client - have your client pointed at a save folder.
Have the client running when you save/load games.

There are three tabs in the client. 

**Play Game Tab**

Here you can select from games that have been created and choose by clicking in the dropown to select one to play.
Also displays state of current game, are we playing or waiting, what round is it, etc.

The Poll Server Checkbox needs to be checked for other players games to be automatically downloaded when they save their game. It can be unchecked to ease strain on the server if you want to keep the client running but are not playing.

If the The Delete Uploaded Saves checkbox is ticked then local copies of uploaded saves will be deleted. This makes sense as in PBEM games you cannot load a turn you have played. Untick to keep them just in case... (Remember, if in doubt always back up saves)

Note: If you are able to save between turns, make sure you do not overwrite the overwrite_me file! When this file is overwritten it is uploaded to the server as the next players download.


**Create Game Tab**

This takes you to the create game page on the website. You will be able to create games on this tab directly when I get
round to it.

**Info Tab**

This tells you  the current state of the client and has a scrolling text log of recent events, which can help inform
in case of difficulty, state of connection to server or issues with settings, etc.


**Toubleshooting**

Check the text in the info tab for hints if things are not working. Click menu>server>check server status and see what
it says.

Make sure the authorisation token is correct.

Make sure the other settings in pbem4se_client.exe are correct.

Make sure you have a game selected and the info in the Play Game Tab seems sane.

Make sure The Poll Server Checkbox is checked.


**The Website**

This is where games are created and shared between players. It acts as a single point of contact between the clients.

There is a bit of help on the website which will be more up-to-date.

There are basically three functions to the website.

1. Create an account and provide authorisation token for the client.

2. Choose people who you can play games with.

3. Create games.


The server itself functions to receive uploaded games from clients and download them. This automatically happens when 
a game is activated and chosen in a client.

There is no function to communicate with other players on the server. It is assumed that you have arranged for a game
on the forum or are otherwise in contact with who you want to play with.

There may be some kind of lobby or player looking for game / starting game thing added if it seems to work.









