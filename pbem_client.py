"""
A helper for playing shadow empire PBEM

See readme for details.

"""
import hashlib
import json
import logging
from logging.handlers import SocketHandler
import os
import re
import tkinter as tk
import tkinter.simpledialog
import webbrowser
from datetime import datetime
from time import strftime
from tkinter import filedialog
from tkinter import messagebox
from comms_lib import ServerHandler
import pygubu
import requests
from helpers_lib import *

# create logger
# logger = logging.getLogger("pbem4se_application")
# datestr = strftime("[%d/%m/%Y %T]")
# logfile = "pbem4se.log"
# logformat = "%(asctime)s %(levelname)s %(message)s"
# logging.basicConfig(
#     filename=logfile,
#     filemode="w",
#     level=logging.DEBUG,
#     format=logformat,
#     datefmt=datestr,
# )

# use cutelog
logger = logging.getLogger("pbem4se_application")
logger.setLevel(1)  # to send all messages to cutelog
socket_handler = SocketHandler("127.0.0.1", 19996)  # default listening address
logger.addHandler(socket_handler)
logger.info("Hello world!")


class CurrentGame(object):
    """
    the currently active game being played.
    Will be created upon player choosing a new game

    represents data on the server.
    handles downloading the next game to load and uploading the players saves.


    """

    def __init__(self, client, pk):
        """
        mpgame is objectified MPGame from server,

        we are a bit like a state machine...
        our state can be:
        idle - we are not active and may be invalid

        playing - waiting for player to save game, we are either the first player or we have received a game from
        the server. when player saves we upload and transitition to waiting.

        waiting - we are waiting for other players to finish,so that we can download a save and transition to playing.




        :param mpgame:
        """
        # below passed, includes leader dict
        self.pk = pk
        self.client = client

        self.state = "idle"

        # create below from leader object - we know who we are
        self.current_leader = None

        # Lets not do anything hasty, hrrrummmm
        # self.check_for_file_update = False

    def refresh_state(self):
        """
        attempt to set state from examining game on server and existing files

        placeholder for logic testing

        it is our turn if game.current turn = our leader.turn_number:

        we are playing if it is our turn, then


        there should be a loadme file and an ovwerwrite me file.

        when turn finished, loadme is uploaded and renamed to archive_me
        overwrite me is uploaded and renamed to played


        we are waiting if:
        it is not our turn...
        there will not be a loadme file or overwrite_me file

        We need to change state to waiting when we upload file.

        We need to change state to playing and create overwrite_me when we download file



        Refresh state checks the state is sane based on some checks, updating as needed.
        We need to call refresh state:
        When user changes current game







        """

        # first lets assume current object is up_to_date... tgcdo
        logger.debug(f"Refreshing game state on enter:{self.state}")
        self.update_me_from_server()
        self.loadme_file_name = self.generate_loadme_file_name()
        self.loadme_file_path = os.path.join(
            self.client.settings.SaveDir, self.loadme_file_name
        )
        this_is_our_turn = self.current_turn == self.current_leader["turn_number"]
        this_is_first_ever_turn = self.current_turn == 1 and self.current_round == 1
        logger.debug(f"game turn:{self.current_turn} - our turn:{this_is_our_turn}")

        # if this_is_first_ever_turn:
        #     self.client.update_log_text(f"\nGame Started. First turn ever!!")
        #     self.state = "waiting"  # for server to have next save
        #     # self.create_overwrite_me_file()
        #     # we need to generate overwrite_me file
        #     # self.state = "playing"
        #     # self.check_for_file_update = True

        if this_is_our_turn:

            # tgcdo move this code to process_loadme_save
            # lets see if we have a named to_load file
            # lets check the server for a save for this round/turn
            self.create_overwrite_me_file()

            # self.check_for_file_update = True
            self.state = "playing"

            if not this_is_first_ever_turn:

                self.client.update_log_text(f"\nChecking for: {self.loadme_file_path}")

                # we could just generate this when selecting game
                # no - it changes with the round

                self.process_loadme_save(self.loadme_file_path)

            else:

                self.client.update_log_text(f"\nFirst turn. Enjoy the game!")

        else:
            # tgcdo helpful game state text here
            self.loadme_file_name = self.generate_loadme_file_name()
            self.state = "waiting"  # for server to have next save

            # self.check_for_file_update = False
            # next thing is watcher.pbem_client.PBEMClient.onTimer

        logger.debug(f"Refreshing game state on exit:{self.state}")
        self.client.update_info_frame()
        self.client.update_player_status_frame()
        self.client.update_game_status_frame()

    def update_me_from_server(self):
        """
        replaces any remotely-shared properties of ths game with values from server.
        takes server object


        :return:
        """

        try:
            j = self.client.server.greet_server()
            profile = j["profile"]
            self.current_username = j["username"]
            game_from_server = self.client.server.get_game(self.pk)

            for test_leader in game_from_server["leader_list"]:

                if test_leader["profile"] == profile:
                    current_leader_pk = int(
                        test_leader["id"]
                    )  # this is a kludge should be user pk
                    self.current_leader_name = test_leader["name"]
                    self.current_leader = DictObj(test_leader)
                    logger.debug(
                        f"\nCurrent Active Leader is: {self.current_leader_name} - {current_leader_pk}"
                    )
                    self.current_leader_pk = current_leader_pk

            # lets try to add these to ourself
            for k, v in game_from_server.items():
                setattr(self, k, v)

        except Exception as e:
            logger.warning(f"\nFailed to update from game server.{e}")
            pass

    # -----------------------------------------------------------------

    def upload_my_save(self):
        """
        uploads save.
        triggered by watcher or manually
                also names it apropriately, etx


        at this point the overwrite_me file has been overwritten
        :return:
        """

        try:

            save_file_path = self.overwrite_me_file_path
            self.client.update_log_text(f"\nUploading:{save_file_path}")

            self.client.server.upload_and_delete_overwriteme_file(
                self.current_leader_pk, self.overwrite_me_file_path
            )
        except Exception as e:
            logger.debug(f"Error in upload_my_save:{e}")

    # -----------------------------------------------------------------

    def process_loadme_save(self, loadme_file_path):
        """
        downloads next save to load.

        also names it apropriately, etx
        :return:
        """

        if not os.path.exists(
            loadme_file_path
        ):  # we have not already downloaded it, or it's been eaten 8p
            # so lets download it...

            url = self.client.server.download_and_save_loadme_file(
                self.current_leader_pk, loadme_file_path
            )
            logger.debug(f"retrieved url:{url}")

    def name_loadme_save(self):
        """
        returns name to save file as once downloaded
        :return:
        """

    def generate_overwriteme_file_name(self):
        """
        returns name forblank file to overwrite in save dir
        :return:
        """
        try:
            gameuid = self.u_id[:4]
            filename = f"{self.current_username}_{gameuid}_round{self.current_round}_overwrite_me.se1"
            self.overwrite_me_file_name = filename

        except Exception as e:
            filename = f"could_not_generate_filename.error: {e}"
        return filename

    def generate_loadme_file_name(self):
        """
        generates string to use as load file name

        save file format is:
                gameuid id first 4 of game u_id, just for filename uniqueness purposes

               username_gameuid_gameround_load_me.se1

        :return:
        """
        try:
            gameuid = self.u_id[:4]

            filename = f"{self.current_username}_{gameuid}_round{self.current_round}_load_me.se1"
            self.loadme_file_name = filename

        except Exception as e:
            filename = f"could_not_generate_filename.error: {e}"
        return filename

    def create_overwrite_me_file(self):
        """

        :return:
        """

        filename = self.generate_overwriteme_file_name()
        file_path = os.path.join(self.client.settings.SaveDir, filename)

        try:
            if not os.path.exists(file_path):
                with open(file_path, "x") as save_file:
                    save_file.write(f"Blank file to overwrite")

            self.overwrite_me_file_path = file_path
            # self.check_for_file_update = True
            self.save_created_timestamp = os.path.getmtime(file_path)
            self.file_first_modified = False

        except Exception as e:
            self.update_log_text(f"error creating overwriteme file:{e}")


class AboutWindow:
    def __init__(self, root):

        window = tk.Toplevel(root)

        label = tk.Label(
            window,
            text="PBEM4SE\n\n An unofficial helper app for playing multiplayer Shadow Empire games.\n"
            "For help see the readme or visit the forum thread...\nhttps://www.matrixgames.com/forums/tm.asp?m=5024246",
        )
        label.pack(fill="x", padx=50, pady=5)

        button_forum = tk.Button(window, text="Visit Forum", command=self.visit_forum)
        button_forum.pack()
        label = tk.Label(
            window,
            text="For support e-mail demangler@gmail.com \n\n\n",
        )
        label.pack(fill="x", padx=50, pady=5)
        button_close = tk.Button(window, text="Close", command=window.destroy)
        button_close.pack()
        label = tk.Label(
            window,
            text="The Shadow Empire site is:\nwww.matrixgames.com/game/shadow-empire\n",
        )
        label.pack(fill="x", padx=50, pady=5)

    def visit_forum(self):
        # self.update_log_text("\nOpening Website...")

        try:
            url = "https://www.matrixgames.com/forums/tm.asp?m=5024246"

            webbrowser.open(url)
            # self.update_log_text(f"\nOpened {url}")

        except:
            pass


class PBEMClient(pygubu.TkApplication):
    """
    The interface.
    """

    def __init__(self, root=None):  # noqa
        self.test_value = 0
        self.root = root
        self.root.minsize(700, 250)

        # ticker settings
        self.tick_interval = 1000  # milliseconds
        self.tick_count = 0
        self.test_tick_delay = 5  # for ticker test. Seconds
        self.server_poll_delay = 10  # Setting this low will strain server to no purpose. 60 is absolute minimum needed.
        self.last_server_poll_time = 0

        self.test_tick_time = 0

        # init for gui
        self.toplevel = root.winfo_toplevel()
        self.toplevel.withdraw()
        self._init_before()
        self._create_ui()
        self._init_after()
        self.toplevel.deiconify()

        # logging setup

        self.log_text = self.builder.get_object("txt_log")
        self.logscrollbar = self.builder.get_object("logscrollbar")
        self.log_text["yscrollcommand"] = self.logscrollbar.set
        self.logscrollbar["command"] = self.log_text.yview
        self.log_text.configure(bg="light grey")

        # load settings from disk
        # do we create a settings class for updating settings, etc?
        # fukit...
        self.settings = Settings(self)
        temp_poll = self.builder.get_variable("var_poll_server")
        temp_poll.set(True)
        # create server handler from ^^settings

        self.server = ServerHandler(self)

        # parallel task and file watcher setup
        self.timer = TkRepeatingTask(self.root, self.onTimer, int(self.tick_interval))
        self.timer.start()

    def _create_ui(self):
        # 1: Create a builder
        self.builder = builder = pygubu.Builder()

        # 2: Load an ui file
        # builder.add_from_file("watcher.ui")
        builder.add_from_file("pbem4se.ui")

        # 3: Create the widget using self.root as parent
        self.mainwindow = builder.get_object("mainwindow", self.root)

        # Set main menu
        self.mainmenu = menu = builder.get_object("mainmenu", self.root)
        self.set_menu(menu)

        # setup callback for combo
        self.active_games_dropdown = self.builder.get_object("cmb_select_current_game")
        self.active_games_dropdown.bind(
            "<<ComboboxSelected>>", self.active_games_dropdown_click
        )
        self.loadme_label = self.builder.get_object("lbl_loadme_file")
        self.overwriteme_label = self.builder.get_object("lbl_overwrite_me")

        # Configure callbacks
        builder.connect_callbacks(self)

    def onTimer(self):
        """
        detects and processes save file changes and oher async stuff

        for cross platform we rely on  m_time = os.path.getmtime

        on creating save_file_ovrewrite_me we store the mtime as self.save_creation_timestamp


        then we watch for the mtime of this file to change.
        we save this new time as self.save_modified_timestamp
        then we subtract self.save_modified_timestamp from time_now
        this is time since_last modified.

        when this value > threshold we upload etc

        :return:
        """

        # server_polling for new save to load..
        # if we are waiting for a save
        # every so often, ask the server for it.
        # when it delivers, stop polling, create loadme and wait for save modification

        if True:  # is the universe working - we are dealing with time itself here...?

            self.tick_count += 1

            time_now = int(datetime.now().timestamp())
            if time_now - self.test_tick_time > self.test_tick_delay:

                self.test_tick_time = int(datetime.now().timestamp())

                logger.debug(f"Test tick:{self.tick_count}")
            else:
                # print(f"Tick:{self.tick_count}")
                pass

            # save_file_modification_checking
            try:
                logger.debug(f"game state: {self.current_game.state}")

                if self.current_game.state == "playing":

                    # current_game = self.current_game

                    file_path = self.current_game.overwrite_me_file_path
                    logger.debug(f"checking for file update:{file_path}")

                    current_modified_timestamp = os.path.getmtime(file_path)

                    file_has_been_modified = (
                        current_modified_timestamp
                        != self.current_game.save_created_timestamp
                    )
                    time_now = int(datetime.now().timestamp())
                    if file_has_been_modified:
                        if not self.current_game.file_first_modified:
                            self.current_game.file_first_modified = (
                                current_modified_timestamp
                            )
                            logger.debug(
                                f"first modification detectsd... set first_modified to {current_modified_timestamp}"
                            )
                        logger.debug(
                            f"...current timestamp:{current_modified_timestamp} - first_modofied:{ self.current_game.file_first_modified} - time_now{time_now}"
                        )
                        time_since_last_modified = int(time_now) - int(
                            current_modified_timestamp
                        )
                        logger.debug(
                            f"time since = {time_since_last_modified} - threshold = {self.settings.SaveDetectionInterval} "
                        )
                        if time_since_last_modified > int(
                            self.settings.SaveDetectionThreshold
                        ):
                            logger.debug("Threshold exceeded")
                            logger.debug("Save Ready To Upload")

                            # tgcdo update current_game status and upload

                            self.current_game.upload_my_save()
                            # which should move round/turn forwards
                            # refresh from server
                            # if it worked...
                            #
                            # self.current_game.state = (
                            #     "waiting"  # tgcdo placeholder debug remove!!!
                            # )
                            # self.current_game.state = 'waiting'
                            logger.debug("Post upload refresh.............")
                            self.current_game.refresh_state()

                elif self.current_game.state == "waiting":
                    # poll server for save tgcdo

                    # check if server_poll interval is reached]
                    if time_now - self.last_server_poll_time > self.server_poll_delay:

                        self.last_server_poll_time = time_now

                        if self.builder.get_variable("var_poll_server").get():
                            logger.debug(f"Polling Server:{self.tick_count}")
                            self.current_game.process_loadme_save(
                                self.current_game.loadme_file_path
                            )

                            logger.debug(f"...")
                            self.current_game.refresh_state()
                            self.update_info_frame()

                        else:
                            logger.debug(f"Not Polling Server:{self.tick_count} ")
                    else:

                        pass

            except Exception as e:
                logger.debug(e)

    def update_log_text(self, text):
        """
        puts text in log and moves to end
        :param text:
        :return:
        """
        self.log_text.insert("end", text)
        self.log_text.yview_pickplace("end")
        # logger.info(text)

    def update_info_frame(self):
        """
        updates the info status frame info from various objects

        :return:
        """
        try:
            if self.server.test_server():
                server_address = "OK"
            else:
                server_address = "Fail"

        except Exception as e:
            server_address = f"-----"

        try:
            if self.server.test_user():
                current_username = "OK"
            else:
                current_username = "Fail"
        except:
            current_username = "-----"

        try:
            if self.server.test_current_game(self.current_game):
                game_name = "OK"
            else:
                game_name = "-----"
        except:
            game_name = "-----"

        server_label = self.builder.get_object("lbl_server")
        server_label.configure(text=f"Server: {server_address}")

        player_label = self.builder.get_object("lbl_player")
        player_label.configure(text=f"Player: {current_username}")

        game_label = self.builder.get_object("lbl_game")
        game_label.configure(text=f"Game: {game_name}")

    def update_game_status_frame(self):
        """
        updates the game status frame info from various objects

        :return:
        """
        try:
            game_name = self.current_game.game_name
        except:
            game_name = "-----"

        try:
            current_round = self.current_game.current_round
        except:
            current_round = "-----"

        try:
            current_turn = self.current_game.current_turn
        except:
            current_turn = "-----"

        # try:
        #     state = self.current_game.state
        # except:
        state = "-----"
        # state = "-----"

        name_label = self.builder.get_object("lbl_game_name")
        name_label.configure(text=f"{game_name}")

        round_label = self.builder.get_object("lbl_game_round")
        round_label.configure(text=f"Round: {current_round}")

        turn_label = self.builder.get_object("lbl_game_turn")
        turn_label.configure(text=f"Turn: {current_turn}")

        state_label = self.builder.get_object("lbl_game_state")
        state_label.configure(text=f"{state}")

    def update_player_status_frame(self):
        """
        updates the game status frame info from various objects

        :return:
        """

        try:
            leader_name = self.current_game.current_leader.name
        except:
            leader_name = "-----"

        try:
            empire_name = self.current_game.current_leader.empire_name
        except:
            empire_name = "-----"

        try:
            player_turn = self.current_game.current_leader.turn_number
        except:
            player_turn = "-----"

        try:
            state = self.current_game.state
        except:
            state = "-----"

        leader_label = self.builder.get_object("lbl_leader_name")
        leader_label.configure(text=f"{leader_name}")

        empire_label = self.builder.get_object("lbl_empire_name")
        empire_label.configure(text=f"Empire: {empire_name}")

        leader_turn_label = self.builder.get_object("lbl_player_turn")
        leader_turn_label.configure(text=f"Turn: {player_turn}")

        state_label = self.builder.get_object("lbl_player_state")
        state_label.configure(text=f"{state}")

        if state == "playing":

            logger.debug(
                f"blah..... {self.current_game.current_turn}{self.current_game.current_round}"
            )
            if (
                self.current_game.current_turn == 1
                and self.current_game.current_round == 1
            ):
                self.loadme_label.configure(text=f"Enjoy first turn of game!")
                self.overwriteme_label.configure(
                    text=f"Overwrite:{self.current_game.overwrite_me_file_path}"
                )
            else:
                self.loadme_label.configure(
                    text=f"Load:{self.current_game.loadme_file_path}"
                )
                self.overwriteme_label.configure(
                    text=f"Overwrite:{self.current_game.overwrite_me_file_path}"
                )

        else:
            self.loadme_label.configure(text=f"Nothing to do")
            self.overwriteme_label.configure(text=f"-----------")

    def active_games_dropdown_click(self, event):
        selected = self.active_games_dropdown.get()
        self.selected_game = selected
        logger.debug(f"\nCombo Clicked:{selected}")
        try:
            pk = int(selected.split(":")[0])

            logger.debug(f"Game {pk} selected")
            logger.debug(f"\nGame:{pk} selected")

            self.current_game = CurrentGame(self, pk)

            self.current_game.refresh_state()
            self.update_log_text(f"\nGame:{ self.current_game.game_name } selected")

        except Exception as e:
            logger.warning(f"error {e}")
            self.update_log_text(f"error {e}")

    def on_btn_clicked(self, itemid):

        if itemid == "chk_poll_server":

            value = self.builder.get_variable("var_poll_server")
            # value = poll_obj.chk_poll_server
            if value.get():

                self.update_log_text(
                    f"\nPolling Server. Will detect other players turns."
                )

            else:
                self.update_log_text(
                    f"\nNot polling Server. Will not detect other players turns."
                )

        if itemid == "chk_delete_uploaded":

            value = self.builder.get_variable("var_delete_uploaded")
            # value = poll_obj.chk_poll_server
            if value.get():

                self.update_log_text(f"\nWill delete saves after upload")

            else:
                self.update_log_text(f"\nWill not delete saves after upload")

        if itemid == "btn_clear_log_text":
            self.log_text.delete("1.0", tk.END)
            self.log_text.insert("end", "**Log**")

        if itemid == "btn_get_active_games":
            log_message = self.server.get_active_games()
            self.update_log_text(log_message)
            self.update_info_frame()

        if itemid == "btn_open_create_page":
            self.update_log_text("\nOpening Create Game Page...")

            try:
                url = self.server.website_url()
                create_page_url = f"{url}create_game/"
                # url = self.server_url
                webbrowser.open(create_page_url)
                self.update_log_text(f"\nOpened {create_page_url}")

            except:
                pass

    def on_mfile_item_clicked(self, itemid):
        """
        https://stackoverflow.com/questions/45441885/how-can-i-create-a-dropdown-menu-from-a-list-in-tkinter
        """
        if itemid == "m_server_status":

            # refactor this bit
            if self.server.test_server():
                self.update_log_text('\nServer is alive...')
            else:
                self.update_log_text('\nServer is not responding...')

            log_message =self.server.check_server()
            self.update_log_text(log_message)
            self.update_info_frame()

        # loading settings
        if itemid == "mfile_open":

            filepath = filedialog.askopenfilename(
                initialdir=".",
                title="Select a Settings File",
                filetypes=(("Settings files", "*.json*"), ("all files", "*.*")),
            )

            try:
                log_message = self.settings.load_from_file(filepath)
                log_message = log_message + self.server.update_settings()
                # with open(filename, "r") as f:
                #     self.init_settings = json.load(f)
                # self.settings.SaveDir = self.init_settings["SaveLocation"]

                # messagebox.showinfo('File', f'Updated Settings from: {filepath}')
                log_message = log_message + self.server.get_active_games()

                self.update_log_text(log_message)


            except Exception as e:
                messagebox.showinfo("Ooops", f"failed to load settings:{e}")
                self.update_log_text(f"\nFailed to load settings:{e}")

                logger.warning(f"failed to load settings:{e}")

        if itemid == "m_server_page":
            self.update_log_text("\nOpening Website...")

            try:
                url = self.server.website_url()
                # url = self.server_url
                webbrowser.open(url)
                self.update_log_text(f"\nOpened {url}")

            except:
                pass

        if itemid == "mfile_quit":

            self.quit()

        if itemid == "mfile_choose_dir":

            # label.configure(text="Clicked")
            folder_selected = filedialog.askdirectory(
                title="Enter Savefiles Directory",
            )
            self.settings.SaveDir = folder_selected
            try:
                self.current_game.refresh_state()
            except:
                pass
            self.update_log_text(f"\nchanged save dir to:{folder_selected}")

            logger.info(f"changed save dir to:{folder_selected}")

        if itemid == "m_change_user":
            USER_INP = tkinter.simpledialog.askstring(
                title="Update Token", prompt="Enter Token"
            )
            if USER_INP:
                if is_token_hash(USER_INP):
                    self.update_log_text("\nValid Token format.")
                    self.server.change_user(USER_INP)
                    self.server.greet_server()

                    log_message = self.server.get_active_games()
                    self.update_log_text(log_message)
                    # tgcdo code to keep current game as selected if is a game for new player,
                    # otherwise set to none.

                    # self.current_game.update_me_from_server()
                    # self.current_game.refresh_state()

                    self.current_game = None

                    self.update_info_frame()
                    self.update_game_status_frame()
                    self.update_player_status_frame()

                else:
                    self.update_log_text(f"\n **Invalid Token format**")

    def on_about_clicked(self):
        # messagebox.showinfo(
        #     "About",
        #     "PBEM4SE\nA helper app for multiplayer Shadow Empire \nSee . for help",
        # )

        AboutWindow(self.root)


if __name__ == "__main__":
    root = tk.Tk()
    # root.after(2000, task)
    root.title("PBEM4SE - v0.0.7")
    root.geometry("760x250+10+10")
    # root.resizable(width=False, height=False)
    app = PBEMClient(root)
    app.run()
