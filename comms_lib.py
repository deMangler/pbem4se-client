"""
Library of bits and bobs for network
"""
import hashlib
import json
import logging
import os
from logging.handlers import SocketHandler

import requests


# use cutelog
logger = logging.getLogger("pbem4se_application")
logger.setLevel(1)  # to send all messages to cutelog
socket_handler = SocketHandler("127.0.0.1", 19996)  # default listening address
logger.addHandler(socket_handler)
logger.info("Hello world!")


class ServerHandler(object):
    def __init__(self, client):
        """
        deals with all server comms.


        :param session:
        """
        self.current_username = None
        self.client = client
        self.session = requests.session()
        self.update_settings()
        self.greet_server()
        log_message = self.check_server()
        self.client.update_log_text(log_message)
        # self.server_time()


    def update_settings(self):
        """
        update settings from settings object

        :return:
        """
        log_message = "\nAttempting to update server settings..."
        try:
            self.server_domain = self.client.settings.ServerDomain
            self.server_protocol = self.client.settings.ServerProtocol
            self.server_port = self.client.settings.ServerPort
            self.auth_token = self.client.settings.Authorization
            self.token_header = {"Authorization": f"Token {self.auth_token}"}
            self.server_url = f"{self.server_protocol}{self.server_domain}:{self.server_port}"  # debug insecure tgcdo
            logger.debug(f"Loaded initial server settings.")
            log_message = log_message + 'Success.'
            self.client.update_info_frame()
        except Exception as e:
            logger.debug(f"Error updating server settings:{e}")
            log_message = log_message + 'Failed. See log.'
        return log_message

    def change_user(self, auth_token):
        """
        change user to owner of auth


        :param auth_token:
        :return:
        """

        self.auth_token = auth_token
        self.token_header = {"Authorization": f"Token {self.auth_token}"}



    def greet_server(self):
        """
        authorises with server and returns result

        calls self.client.update_info_frame() so dont call it from that....
        :return:
        """
        log_message = "\nAttempting to authorise with server..."
        try:
            r = self.session.get(
                f"{self.server_url}/shadow_pbem/hello_api/",
                headers=self.token_header,
            )

            j = json.loads(r.text)
            message = j["message"]

            self.current_username = j["username"]

            log_message = log_message + f'Success.\n Server says {message}'
            self.client.update_info_frame()
            return j

        except Exception as e:
            log_message = log_message + "Failed. See log."

        return j


    def check_server(self):
        """
        authorises with server and returns result

        calls self.client.update_info_frame() so dont call it from that....
        :return:
        """
        log_message = "\nAttempting to authorise with server..."
        try:
            r = self.session.get(
                f"{self.server_url}/shadow_pbem/hello_api/",
                headers=self.token_header,
            )

            j = json.loads(r.text)
            message = j["message"]

            self.current_username = j["username"]

            log_message = log_message + f'Success.\n Server says {message}'
            self.client.update_info_frame()
            return log_message

        except Exception as e:
            log_message = log_message + "Failed. See log."

        return log_message

    def test_server(self):
        """
        test if server is online

        Should not depend on auth
        """

        test = False  # yeah, I know...
        try:
            t = self.session.get(f"{self.server_url}")

            test = t.status_code == 200
        except Exception as e:

            logger.debug(f"\nFailed server sync checks:{e}")
            test = False
            # client.messagebox.showinfo('Failed ServerTest', e)
        return test

    def test_user(self):
        """
        test if user is authenticated
        """
        test = False
        try:
            r = self.session.get(
                f"{self.server_url}/shadow_pbem/hello_api/",
                headers=self.token_header,
            )
            test = r.status_code == 200

        except Exception as e:

            logger.debug(f"\nFailed server checks: {e}")
            test = False
        return test

    def test_current_game(self, game):
        """
        test if passed game is on server and ok, or somwething

        """
        test = False

        try:

            pk = game.pk
            r = self.session.get(
                f"{self.server_url}/shadow_pbem/game_detail_api/{pk}",
                headers=self.token_header,
            )

            test = r.status_code == 200

            return test

        except Exception as e:

            return test  # yeah, I know...

    def server_time(self):
        test = False  # yeah, I know...
        try:
            t = self.session.get(
                f"{self.server_url}/shadow_pbem/server_time/",
                headers=self.token_header,
            )
            self.client.update_log_text(f"\nTesting server time:{t.text}")
            test = True
        except Exception as e:

            logger.debug(f"\nFailed server sync checks:{e}")
            test = False
            # client.messagebox.showinfo('Failed ServerTest', e)
        return test

    def website_url(self):
        website_url = f"{self.server_url}/shadow_pbem/"
        return website_url

    def download_and_save_loadme_file(self, pk, loadme_file_path):
        """
        downloads the url for the save to be loaded by this leader pk


        :param pk:
        :return:
        """
        log_message = "\nAttempting to download next save..."
        try:

            r = self.session.get(
                f"{self.server_url}/shadow_pbem/get_next_loadgame/{pk}",
                headers=self.token_header,
            )

            r_json = json.loads(r.text)

            url = r_json["url"]

            save_url = f"{self.server_url}/{url}"
            try:

                f = self.session.get(
                    save_url,
                    headers=self.token_header,
                )

                with open(loadme_file_path, "wb").write(f.content) as save_file:
                    self.client.update_log_text(f"created file {loadme_file_path}")
            except Exception as e:
                logger.debug(f"{e}")


            log_message = log_message+f'Success:{url}'



        except Exception as e:
            self.client.update_log_text(
                f"\nError getting save from server - see log file for details"
            )
            logger.debug(f"\nFailed to get url from server.{e}")
            log_message = log_message + f'Failed - see log'

        return  log_message

    def upload_and_delete_overwriteme_file(self, leader_pk, save_file_path):
        """

        :param pk:
        :param overwriteme_file_path:
        :return:
        """

        with open(save_file_path, "rb") as test_save_file:

            test_save_file_hash = hashlib.md5(test_save_file.read()).hexdigest()

            # self.log_text.yview_pickplace("end")

            test_files = {"file": ("upload_file", test_save_file, "application/zip")}

            test_save_file.seek(0, os.SEEK_END)
            filesize = test_save_file.tell()
            test_save_file.seek(0)

            self.client.update_log_text(f"\nUploading save File size:{filesize}")

            r = self.session.put(
                f"{self.server_url}/shadow_pbem/savefile_api/",
                files=test_files,
                data={
                    "hash": test_save_file_hash,
                    "leader": leader_pk,
                },
                headers=self.token_header,
            )
            if r.status_code == 200:
                self.client.update_log_text(
                    f"\n[{r.status_code}]Passed Save File Upload checks:{r.text}"
                )

                # If we are here we uploaded file
                if self.client.builder.get_variable("var_delete_uploaded").get():
                    try:
                        uploaded_file_path = save_file_path
                        os.remove(uploaded_file_path)
                        self.client.update_log_text(f"\n[{save_file_path} cleaned")

                    except OSError as e:
                        logger.debug(f"Error++:{e}")

                # delete overwrite_me file and save. and
                # rest vars so that file watcher does not spam server and stuff
                # self.watch_save should only be true if we have a downloaded overwrite_me file, we are conneced
                # to the server and the server reports that it is our turn (that matches overwrite_me round)

                self.client.update_log_text(f"\n[{save_file_path} uploaded to server")

            else:
                self.client.update_log_text(
                    f"\n[{r.status_code}]Failed Save File Upload save file:{r.text}"
                )

    def get_game(self, pk):
        """
        returns game dict from server
        :param pk:
        :return:
        """

        try:

            r = self.session.get(
                f"{self.server_url}/shadow_pbem/game_detail_api/{pk}",
                headers=self.token_header,
            )

            r_json = json.loads(r.text)

            logger.debug(f"\nGot game from server.")

            return r_json

        except Exception as e:
            self.client.update_log_text(f"\nFailed to get game from server.{e}")
            return  # yeah, I know...

    def get_active_games(self):
        """

        :return:
        """
        log_message = "\nAttempting to retrieve active games from server..."
        good_token = self.auth_token
        good_token_header = {"Authorization": f"Token {good_token}"}
        hello_url = f"{self.server_url}"
        cmb_box = self.client.active_games_dropdown
        try:
            r = self.session.get(
                f"{hello_url}/shadow_pbem/active_games_for_user_api/",
                headers=good_token_header,
            )

            r_json = json.loads(r.text)
            active_games_list = r_json["active_games"]

            # creating list of unique values for cb = we can update this here... not naughty at all
            game_data = []
            game_index = 1

            for game in active_games_list:
                game_data.append(f"{game}")
                game_index += 1


            log_message = log_message + f'Success. '


            if len(game_data) > 0:
                cmb_box["values"] = game_data
                log_message = log_message + f'{len(game_data)} retrieved.'
            else:
                log_message = log_message + 'However, you have no active games'
                cmb_box["values"] = [""]
                cmb_box.set("")
                self.client.current_game = None
                self.client.update_game_status_frame()
                self.client.update_player_status_frame()

        except Exception as e:
            log_message = log_message + 'Failed - see log.'
            logger.debug(f"\nFailed  fetching active games:{e}")
            cmb_box["values"] = [""]
            cmb_box.set("")
            self.client.current_game = None
            self.client.update_game_status_frame()
            self.client.update_player_status_frame()

        return log_message
